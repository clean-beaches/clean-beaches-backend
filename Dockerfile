# This Dockerfile is used in order to build a container that runs our spring app
FROM openjdk:17-bullseye
ARG RUN_JAVA_VERSION=1.3.8
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en'

# install curl in debian image
RUN apt-get install curl && apt-get clean autoclean

# use run file from fabric8
RUN mkdir /deployments \
    && chown 1001 /deployments \
    && chmod "g+rwX" /deployments \
    && chown 1001:root /deployments \
    && curl https://repo1.maven.org/maven2/io/fabric8/run-java-sh/${RUN_JAVA_VERSION}/run-java-sh-${RUN_JAVA_VERSION}-sh.sh -o /deployments/run-java.sh \
    && chown 1001 /deployments/run-java.sh \
    && chmod 540 /deployments/run-java.sh

# disable stack trace optimization to log each stack trace
ENV JAVA_OPTIONS="-XX:-OmitStackTraceInFastThrow"

COPY /build/libs/*.jar /deployments/app.jar

EXPOSE 8081

ENTRYPOINT [ "/deployments/run-java.sh" ]
