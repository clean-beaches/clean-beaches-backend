package earth.clean_beaches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleanBeachesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CleanBeachesApplication.class, args);
	}

}
