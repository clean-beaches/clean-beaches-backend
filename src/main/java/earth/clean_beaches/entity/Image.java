package earth.clean_beaches.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Setter
@Getter
public class Image {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @OneToOne
    private ImageData imageData;

    private LocalDateTime date;

    @Embedded
    private Location location;

    @ManyToOne(fetch = FetchType.LAZY)
    private Author author;

    @Column(length = 64)
    private String imageType;
}
