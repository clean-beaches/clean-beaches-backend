package earth.clean_beaches.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import earth.clean_beaches.entity.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
}
