package earth.clean_beaches.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import earth.clean_beaches.entity.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
}
