CREATE TABLE author
(
    id            UUID NOT NULL,
    email         varchar(255) NULL,
    "name"        varchar(255) NULL,
    register_date timestamp NULL,
    CONSTRAINT author_pkey PRIMARY KEY (id)
);

CREATE TABLE image
(
    id            UUID NOT NULL,
    "date"        timestamp NULL,
    image_type    varchar(64) NULL,
    latitude      int8 NULL,
    longitude     int8 NULL,
    author_id     int8 NULL,
    image_data_id int8 NULL,
    CONSTRAINT image_pkey PRIMARY KEY (id)
);

CREATE TABLE image_data
(
    id        UUID NOT NULL,
    "content" bytea NULL,
    image_id  int8 NULL,
    CONSTRAINT image_data_pkey PRIMARY KEY (id)
);